package com.jefferson.salvadore.bank_application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Nonnull;

import com.jefferson.salvadore.bank_application.account.BankAccount;

public class DBService {

  private final Connection connection;

  public DBService() {
    connection = DBConnection.makeConnection("jdbc:mysql://localhost:3306/bank?useSSL=false",
        "root", "password");
  }

  public void getResults(@Nonnull final String getStuff) throws SQLException {
    final PreparedStatement getData = connection.prepareStatement(getStuff);
    final ResultSet results = getData.executeQuery();

    while (results.next()) {
      System.out.print(results.getString("cust_id"));
      System.out.print(" | ");
      System.out.print(results.getString("f_name"));
      System.out.print(" | ");
      System.out.print(results.getString("l_name"));

    }
    results.close();
  }

  public void insert(@Nonnull final BankAccount accountToInsert) throws SQLException {

    final String customerData = "INSERT INTO customer (cust_id, f_name, l_name) VALUES (?, ?, ?)";
    final PreparedStatement insertCustomer = connection.prepareStatement(customerData);
    insertCustomer.setInt(1, accountToInsert.getCustomer().getId());
    insertCustomer.setString(2, accountToInsert.getCustomer().getFirstName());
    insertCustomer.setString(3, accountToInsert.getCustomer().getLastName());

    final String sAccountData =
        "INSERT INTO savings_acc (cust_id, balance, acc_num) VALUES (?, ?, ?)";
    final String cAccountData =
        "INSERT INTO checking_acc (cust_id, balance, acc_num) VALUES (?, ?, ?)";
    final PreparedStatement insertAccount;

    if (accountToInsert.getAccountType().equals("Savings Account")) {
      insertAccount = connection.prepareStatement(sAccountData);
    } else {
      insertAccount = connection.prepareStatement(cAccountData);
    }

    insertAccount.setInt(1, accountToInsert.getCustomer().getId());
    insertAccount.setDouble(2, accountToInsert.getBalance());
    insertAccount.setInt(3, accountToInsert.getAccountNumber());

    insertCustomer.execute();
    insertAccount.execute();

    insertCustomer.close();
    insertAccount.close();
  }

  public void dropEntry(@Nonnull final String enteryToDrop) throws SQLException {
    final PreparedStatement drop = connection.prepareStatement(enteryToDrop);
    drop.execute();
    drop.close();
  }

  public void updateEntry(@Nonnull final String updateString) throws SQLException {
    final PreparedStatement update = connection.prepareStatement(updateString);
    update.executeUpdate();
    update.close();
  }
}
