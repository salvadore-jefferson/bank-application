// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application;

import static com.jefferson.salvadore.bank_application.account.AccountType.SAVINGS;

import com.jefferson.salvadore.bank_application.account.Account;
import com.jefferson.salvadore.bank_application.accountmanager.AccountManager;
import com.jefferson.salvadore.bank_application.accountmanager.InMemoryAccountManager;

/**
 * The application class for working with bank accounts
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public class Bank {

  public static void main(String[] args) {

    final AccountManager manager = InMemoryAccountManager.getInstance();

    manager.openAccount(new Account(new Customer("Sal", "Jefferson"), 1000.00, SAVINGS));
    manager.openAccount(new Account(new Customer("Jim", "Jones"), 1002.00, SAVINGS));
    manager.printAllAccounts();
    manager.closeAccount(97);
    manager.closeAccount(15);
    manager.printAllAccounts();
    System.out.println(manager.getCurrentAccountBalance(2));
    manager.writeToDatabase(new DBService());
  }
}
