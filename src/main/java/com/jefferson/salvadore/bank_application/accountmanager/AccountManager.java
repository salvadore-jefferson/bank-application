// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application.accountmanager;

import javax.annotation.Nonnull;

import com.jefferson.salvadore.bank_application.DBService;
import com.jefferson.salvadore.bank_application.account.BankAccount;

/**
 * An Interface for managing {@link BankAccount}s, for the Bank application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public interface AccountManager {

  /**
   * Opens a new {@link BankAccount} to the bank's account list
   * 
   * @param newAccount
   * @return True if the account was added successfully. Returns false if the account already exists
   *         or if there was an error while processing.
   */
  boolean openAccount(BankAccount newAccount);

  /**
   * Add a new {@link BankAccount} to an existing customer
   * 
   * @param newAccount
   * @return True if the account has been added. False if the account already exists or if there was
   *         an error while processing.
   */
  boolean addAccount(int costumerId, double deposit);

  /**
   * Closes the {@link BankAccount} passed as an argument if it exists.
   * 
   * @param accountNumber
   * @return True if the account was closed successfully. False if the account does not exist, or if
   *         there was an error while processing
   */
  boolean closeAccount(int accountNumber);

  /**
   * Returns the current balance for the requested {@link BankAccount}
   * 
   * @param accountNumber
   * @return Returns the current account balance. Or zero(0) if the account does not exist.
   */
  double getCurrentAccountBalance(int accountNumber);

  /**
   * Print the details for all {@link BankAccount}s in the list.
   */
  void printAllAccounts();

  /**
   * Implement this method in a concrete class to write all {@account} data to the database.
   * 
   * @param service DBService
   */
  default void writeToDatabase(@Nonnull final DBService service) {
    throw new UnsupportedOperationException("Implement in concrete class");
  }
}
