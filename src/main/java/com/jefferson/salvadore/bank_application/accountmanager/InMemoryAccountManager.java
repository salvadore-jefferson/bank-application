// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application.accountmanager;

import static com.jefferson.salvadore.bank_application.account.AccountType.SAVINGS;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Nonnull;

import com.jefferson.salvadore.bank_application.Customer;
import com.jefferson.salvadore.bank_application.DBService;
import com.jefferson.salvadore.bank_application.account.Account;
import com.jefferson.salvadore.bank_application.account.BankAccount;

/**
 * An in memory implementation of the {@link AccountManager} interface, for storing
 * {@link BankAccount}s to a virtual database and accessing {@code Account} details
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public class InMemoryAccountManager implements AccountManager {

  public static final InMemoryAccountManager MEMORY_INSTANCE = new InMemoryAccountManager();
  private static final Set<BankAccount> ACCOUNT_LIST = new HashSet<>();

  /**
   * This is a Singleton class. Only one {@code InMemoryAccountManager} per application
   */
  private InMemoryAccountManager() {}

  @Override
  public boolean openAccount(@Nonnull final BankAccount newAccount) {
    return ACCOUNT_LIST.add(newAccount);
  }

  @Override
  public boolean addAccount(final int customerId, final double deposit) {
    for (Iterator<BankAccount> iter = ACCOUNT_LIST.iterator(); iter.hasNext();) {
      final Customer customer = iter.next().getCustomer();
      if (customer.getId() == customerId) {
        return ACCOUNT_LIST.add(new Account(customer, deposit, SAVINGS));
      }
    }
    return false;
  }

  @Override
  public boolean closeAccount(final int accountNumber) {
    for (Iterator<BankAccount> iter = ACCOUNT_LIST.iterator(); iter.hasNext();) {
      final BankAccount account = iter.next();
      if (account.getAccountNumber() == accountNumber) {
        return ACCOUNT_LIST.remove(account);
      }
    }
    return false;
  }

  @Override
  public double getCurrentAccountBalance(final int accountNumber) {
    double balance = 0;
    for (Iterator<BankAccount> iter = ACCOUNT_LIST.iterator(); iter.hasNext();) {
      final BankAccount account = iter.next();
      if (account.getAccountNumber() == accountNumber) {
        balance = account.getBalance();
      }
    }
    return balance;
  }

  @Override
  public void printAllAccounts() {
    for (Iterator<BankAccount> iter = ACCOUNT_LIST.iterator(); iter.hasNext();) {
      final BankAccount account = iter.next();
      System.out.println(account.toString());
    }
  }

  @Override
  public void writeToDatabase(@Nonnull final DBService service) {
    for (Iterator<BankAccount> iter = ACCOUNT_LIST.iterator(); iter.hasNext();) {
      final BankAccount account = iter.next();
      try {
        service.insert(account);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Get the Singleton instance of this class
   * 
   * @return InMemoryAccountManager instance
   */
  public static InMemoryAccountManager getInstance() {
    return MEMORY_INSTANCE;
  }


}
