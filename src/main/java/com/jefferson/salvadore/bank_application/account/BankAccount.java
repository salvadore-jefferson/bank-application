// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application.account;

import com.jefferson.salvadore.bank_application.Customer;

/**
 * An interface for defining Account objects, for use in the Bank application.
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public interface BankAccount {

  /**
   * Make a withdrawal of the specified amount from the account.
   * 
   * @param ammountToWithdrawl.
   * @return The new balance after the transaction.
   */
  double makeWithdrawal(double ammountToWithdrawal);

  /**
   * Make a deposit of the specified amount to the account.
   * 
   * @param amountToDeposit.
   * @return The new balance after the transaction.
   */
  double makeDeposit(double amountToDeposit);

  /**
   * Check the current balance of the account.
   * 
   * @return The current balance of the account.
   */
  double getBalance();

  /**
   * Get the type of this account.
   * 
   * @return The account type as a String.
   */
  String getAccountType();

  /**
   * Gets the account number for this account.
   * 
   * @return int
   */
  int getAccountNumber();

  /**
   * Get the {@Customer} that holds this account.
   * 
   * @return {@code Customer}
   */
  Customer getCustomer();
}
