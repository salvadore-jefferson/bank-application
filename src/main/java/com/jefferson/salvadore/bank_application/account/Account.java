// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application.account;

import java.util.Objects;

import javax.annotation.Nonnull;

import com.jefferson.salvadore.bank_application.Customer;

/**
 * An implementation of the {@link BankAccount} interface to represent savings accounts.
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public class Account implements BankAccount {

  private double balance;
  private final Customer customer;
  private final int acc_num = (int) (Math.random() * 100) + 10;
  private final AccountType accountType;

  public Account(@Nonnull final Customer customer, final double balance,
      @Nonnull final AccountType accountType) {
    this.customer = customer;
    this.balance = balance;
    this.accountType = accountType;
  }

  @Override
  public double makeWithdrawal(double ammountToWithdrawal) {
    balance = balance - ammountToWithdrawal;
    return balance;
  }

  @Override
  public double makeDeposit(double amountToDeposit) {
    balance = balance + amountToDeposit;
    return balance;
  }

  @Override
  public double getBalance() {
    return balance;
  }

  @Override
  public String getAccountType() {
    return accountType.getType();
  }

  @Override
  public int getAccountNumber() {
    return acc_num;
  }

  @Override
  public Customer getCustomer() {
    return customer;
  }

  @Override
  public boolean equals(final Object otherAccount) {
    if (this == otherAccount) {
      return true;
    }
    if (!(otherAccount instanceof Account)) {
      return false;
    }
    final Account account = (Account) otherAccount;
    return this.getAccountNumber() == account.getAccountNumber()
        && this.getCustomer().getFirstName().equals(account.getCustomer().getFirstName())
        && this.getCustomer().getLastName().equals(account.getCustomer().getLastName())
        && this.getCustomer().getId() == account.getCustomer().getId();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getAccountNumber(), this.getCustomer().getFirstName(),
        this.getCustomer().getId());
  }

  @Override
  public String toString() {
    final String details = "Account Holder: " + this.getCustomer().getFirstName() + " "
        + this.getCustomer().getLastName() + " | Account Type: " + this.getAccountType()
        + " | Current Balance: $" + this.getBalance() + " | Account Number: "
        + this.getAccountNumber();

    return details;

  }


}
