// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application.account;

import com.jefferson.salvadore.bank_application.Bank;

/**
 * A set of Enums to represent different account types used by the {@link Bank} application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-24-16
 *
 */
public enum AccountType {

  SAVINGS("Savings Account"),

  CHECKING("Checking Account");

  private final String type;

  private AccountType(final String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
