package com.jefferson.salvadore.bank_application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.Nonnull;

public class DBConnection {

  public static Connection conn;

  private DBConnection() {}

  public static Connection makeConnection(@Nonnull final String dbUrl, @Nonnull final String user,
      @Nonnull final String pass) {
    try {
      conn = DriverManager.getConnection(dbUrl, user, pass);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return conn;
  }

  public static void close() {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
