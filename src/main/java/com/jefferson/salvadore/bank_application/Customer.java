// Copyright © 2016 Salvadore Jefferson
package com.jefferson.salvadore.bank_application;

import javax.annotation.Nonnull;

/**
 * A POJO for representing customers in the Bank application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 5-23-16
 *
 */
public class Customer {

  private static int instanceCount = 1;
  private final String firstName;
  private final String lastName;
  private int cust_id;

  public Customer(@Nonnull final String firstName, @Nonnull final String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    cust_id = instanceCount;
    instanceCount++;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getId() {
    return cust_id;
  }

}
